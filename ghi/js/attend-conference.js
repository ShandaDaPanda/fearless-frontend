window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
        // Here, add the 'd-none' class to the loading icon
        let spinner = document.querySelector('#loading-conference-spinner');
        spinner.classList.add('d-none')
        // Here, remove the 'd-none' class from the select tag
        let selector = document.querySelector('#conference');
        selector.classList.remove('d-none');

        const formTag = document.getElementById('create-attendee-form'); //idchanged
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData.entries()));

            const locationUrl = 'http://localhost:8001/api/attendees/';// url changed
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };


            let dogForm = document.getElementById('create-attendee-form');
            dogForm.classList.add('d-none')
            // Here, remove the 'd-none' class from the select tag
            let successTag = document.getElementById('success-message');
            successTag.classList.remove('d-none');


            selector.classList.remove('d-none');
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
                console.log(newConference);
            }
        });
    }
});
